# State management.

Es un manejador de estado global, que nos apoya a mantener la información concentrada en uno o varios lugares para poder consumirla en cualquier componente de nuestra aplicación cuando es requerido.

El state management es un super objeto {} que almacena información que sera consumida por los componentes.

¿Que state management existen?

- Redux,
- Context.API,
- Zustand,
- Recoil,
- Jotai,
- Mobx

Los tres primeros los veremos en este curso y son los tres mas populares.

¿Deberia usar siempre un stete managent?

Depende, pero seria bien seguir estos pasos:

1. Usa estado local hasta que puedas.
2. Si tu aplicación empieza a crecer y necesitas actualizar la información y que se actualice en componentes no relativos.
3. Si tu aplicación tiene demasiada información que venga de la API.
4. Si, pero elige el adecuado para tu proyecto.
